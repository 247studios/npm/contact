const assert = require('assert');
const querystring = require('querystring');
const { URL } = require('url');
const contact = require('..');

const arg0 = {
  from: 'sender@example.com',
  to: 'receiver@example.com',
  subject: 'Contact Form',
  provider: 'sendgrid',
  redirectSuccess: 'http://example.com',
  redirectError: 'http://example.com',
  template: ({
    name,
    email,
    message,
  }) => `
    ${name} &lt;${email}&gt; sent you a message.
    <br>
    ${message}
  `,

  logger: {
    log: () => {},
    warn: () => {},
    error: () => {},
  },

  _dependencies: {
    recaptcha(gRecaptchaResponse) {
      return new Promise(resolve => {
        resolve(JSON.stringify({
          success: isRecaptchaSuccess, // global, set this to what ever you want
        }));
      })
    },

    mail: {
      sendgrid: function (params) {
        const {
          from,
          to,
          subject,
          replyTo,
          html,
        } = params;
        return new Promise((resolve) => {
          resolve({
            error: mailSendgridError,
          });
        });
      }
    },
  }
};

describe('247studios Contact', function () {
  beforeEach(function () {
    isRecaptchaSuccess = true;
    mailSendgridError = null;
  });

  describe('Specification', function () {
    it('is a function', function () {
      assert.strictEqual(typeof contact, 'function');
    });
    
    it('returns a function', function () {
      assert.strictEqual(typeof contact(arg0), 'function');
    });
  });

  describe('Specification of returned function', function () {
    const contactFn = contact(arg0);
    let body;

    beforeEach(function () {
      body = {
        name: 'John Doe',
        email: 'john@example.com',
        message: 'Hello my name is John',
        'g-recaptcha-response': 'some recaptcha response',
      };
    });
    
    it("fails when `httpMethod` is 'GET'", function (done) {
      contactFn({
        httpMethod: 'GET', // should be 'POST'
      }, {}, function(err, res) {
        assert.ok(!err);
        assert.strictEqual(res.statusCode, 405);
        done();
      });
    });
    
    it("requires `httpMethod` is 'POST'", function (done) {
      contactFn({
        httpMethod: 'POST',
      }, {}, function(err, res) {
        assert.ok(!err);
        assert.notStrictEqual(res.statusCode, 405);
        done();
      });
    });
    
    it("fails when `body.name` is missing", function (done) {
      delete body.name;
      contactFn({
        httpMethod: 'POST',
        body: querystring.stringify(body),
      }, {}, function(err, res) {
        assert.ok(!err);
        const result = new URL(res.headers.Location).searchParams;
        assert.deepStrictEqual(JSON.parse(result.get('errorCodes')), ['InvalidBody']);
        done();
      });
    });

    it("fails when `body.email` does not look like an email", function (done) {
      body.email = 'obviously not an email';
      contactFn({
        httpMethod: 'POST',
        body: querystring.stringify(body),
      }, {}, function(err, res) {
        assert.ok(!err);
        const result = new URL(res.headers.Location).searchParams;
        assert.deepStrictEqual(JSON.parse(result.get('errorCodes')), ['InvalidEmail']);
        done();
      });
    });

    it("requires `body` to be a querystring as a 'string' containing `name`, `email`, and `message`", function (done) {
      contactFn({
        httpMethod: 'POST',
        body: querystring.stringify(body),
      }, {}, function(err, res) {
        assert.ok(!err);
        const result = new URL(res.headers.Location).searchParams;
        assert.notDeepStrictEqual(JSON.parse(result.get('errorCodes')), ['InvalidBody']);
        done();
      });
    });

    it("fails when recaptcha validation does not pass", function (done) {
      body['g-recaptcha-response'] = 'some invalid recaptcha response';
      isRecaptchaSuccess = false; // mock recaptcha method to fail
      contactFn({
        httpMethod: 'POST',
        body: querystring.stringify(body),
      }, {}, function(err, res) {
        assert.ok(!err);
        const result = new URL(res.headers.Location).searchParams;
        assert.deepStrictEqual(JSON.parse(result.get('errorCodes')), ['RecaptchaError']);
        done();
      });
    });

    it("fails when mail function fails", function (done) {
      // mock mail method to fail
      mailSendgridError = new Error('Potato');
      contactFn({
        httpMethod: 'POST',
        body: querystring.stringify(body),
      }, {}, function(err, res) {
        assert.ok(!err);
        const result = new URL(res.headers.Location).searchParams;
        assert.deepStrictEqual(JSON.parse(result.get('errorCodes')), ['MailError']);
        done();
      });
    });
  });
});