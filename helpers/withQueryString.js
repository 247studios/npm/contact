const { URL, URLSearchParams } = require('url');

module.exports = function withQueryString(urlStr, params) {
  const url = new URL(urlStr);
  
  var searchParams = new URLSearchParams(url.searchParams);
  Object.keys(params).forEach((key) => {
    const value = params[key];
    searchParams.set(key, String(value));
  });
  
  return url.href.replace(/\?.*$/, '') + '?' + searchParams.toString();
}