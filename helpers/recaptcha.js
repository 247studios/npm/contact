const querystring = require('querystring');
const request = require('./request');

module.exports = function recaptcha(response, remoteip) {
  const postData = querystring.stringify({
    secret: process.env.RECAPTCHA_SECRET,
    response,
    remoteip,
  });
  return request({
    hostname: 'www.google.com',
    port: 443,
    path: '/recaptcha/api/siteverify',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': postData.length
    },
  }, postData);
}
