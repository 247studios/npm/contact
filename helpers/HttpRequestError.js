module.exports = class HttpRequestError extends Error {
  /**
   * @param {number} statusCode
   * @param {Array<string>} errorCodes
   */
  constructor(statusCode, errorCodes) {
    super(errorCodes.join(', '));
    this.statusCode = statusCode;
    this.errorCodes = errorCodes;
  }
}
