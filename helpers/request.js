const https = require('https');

module.exports = function request(options, postData) {
  return new Promise((resolve, reject) => {
    const req = https.request(options, (response) => {
      const body = [];
      response.on('data', (chunk) => body.push(chunk));
      response.on('end', () => resolve(body.join('')));
    });
    req.on('error', reject);
    if (postData) {
      req.write(postData);
    }
    req.end();
  })
}
