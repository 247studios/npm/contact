# @247studios/contact

Netlify Function generator that sends an email using SendGrid.

`npm install @247studios/contact`

# Usage

```js
const contact = require('@247studios/contact');

exports.handler = contact({
  from: 'sender@example.com',
  to: 'receiver@example.com',
  subject: 'Operation - Leave Request',
  redirectSuccess: 'http://example.com',
  redirectError: 'http://example.com',
  provider: 'sendgrid',
  template: ({
    name,
    email,
    message,
  }) => `<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
</head>
<body>
  ${name} &lt;${email}&gt; says:
  <br>
  ${message}
</body>
</html>`,
});
```

# Environment Variables

| | | 
|-|-| 
| `RECAPTCHA_SECRET` | [https://developers.google.com/recaptcha](https://developers.google.com/recaptcha) |
| `SENDGRID_API_KEY` | [https://sendgrid.com/docs/ui/account-and-settings/api-keys/](https://sendgrid.com/docs/ui/account-and-settings/api-keys/) |

When deploying to Netlify, set the [Build Environment Variables](https://www.netlify.com/docs/continuous-deployment/#build-environment-variables).

# Releasing New Versions

Specify semantic version change by running one of `npm version patch`,
`npm version minor`, or `npm version major`.

Once commit reaches the `master` branch on GitLab, the release will be deployed
to npm automatically.
