const request = require('../helpers/request');

/**
 * Sends mail using Sendgrid's API.
 */
module.exports = async function mail({
  to,
  from,
  reply_to,
  subject,
  html,
  text,
}) {
  const postData = JSON.stringify({
    personalizations: [{
      to: [{ email: to }],
    }],
    from: { email: from },
    reply_to,
    subject: subject,
    content: [
      text && {
        type: 'text/plain',
        value: text
      },
      html && {
        type: 'text/html',
        value: html
      },
    ].filter(x => x),
  });
  const body = JSON.parse((await request({
    hostname: 'api.sendgrid.com',
    port: 443,
    path: '/v3/mail/send',
    method: 'POST',
    headers: {
      Authorization: 'Bearer ' + process.env.SENDGRID_API_KEY,
      'Content-Type': 'application/json',
    },
  }, postData)) || "{}");
  if (body.errors) {
    return {
      error: {
        errorCodes: body.errors,
      },
    };
  }
  return {
    body,
  };
}
