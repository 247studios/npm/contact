const querystring = require('querystring');
let recaptcha = require('./helpers/recaptcha');
const HttpRequestError = require('./helpers/HttpRequestError');
const isEmail = require('./helpers/isEmail');
const withQueryString = require('./helpers/withQueryString');
let mail = require('./mail');

/**
 * Creates a Netlify serverless function that sends a mail.
 * 
 * Method: `POST`
 * Body: `name`, `email`, `message`, `g-recaptcha-response`
 * 
 * Expects the environment variables: `RECAPTCHA_SECRET`, `SENDGRID_API_KEY`
 * 
 * @param {Object} params
 * @param {string} params.from
 * @param {string} params.to
 * @param {string} params.subject
 * @param {string} params.provider
 * @param {function} params.template
 * @param {string} params.redirectSuccess
 * @param {string} params.redirectError
 * @param {Object} [params.logger]
 * @param {function} params.logger.log
 * @param {function} params.logger.warn
 * @param {function} params.logger.error
 * @param {Object} [params._dependencies]
 * @param {function} [params._dependencies.recaptcha]
 * @param {Object} [params._dependencies.mail]
 * @param {function} params._dependencies.mail.sendgrid
 */
module.exports = (params) => {
  if (typeof params !== 'object') throw new TypeError("`arguments[0]` must be an 'object'");
  if (typeof params.from !== 'string') throw new TypeError("`arguments[0].from` must be a 'string'");
  if (typeof params.to !== 'string') throw new TypeError("`arguments[0].to` must be a 'string'");
  if (typeof params.subject !== 'string') throw new TypeError("`arguments[0].subject` must be a 'string'");
  if (typeof params.provider !== 'string') throw new TypeError("`arguments[0].provider` must be a 'string'");
  if (typeof params.template !== 'function') throw new TypeError("`arguments[0].template` must be a 'function'");
  if (typeof params.redirectSuccess !== 'string') throw new TypeError("`arguments[0].redirectSuccess` must be a 'string'");
  if (typeof params.redirectError !== 'string') throw new TypeError("`arguments[0].redirectError` must be a 'string'");
  if (params.logger !== undefined) {
    if (typeof params.logger !== 'object') throw new TypeError("`arguments[0].logger` must be an 'object'");
    if (typeof params.logger.log !== 'function') throw new TypeError("`arguments[0].logger.log` must be a 'function'");
    if (typeof params.logger.warn !== 'function') throw new TypeError("`arguments[0].logger.warn` must be a 'function'");
    if (typeof params.logger.error !== 'function') throw new TypeError("`arguments[0].logger.error` must be a 'function'");
  }
  const {
    from,
    to,
    subject,
    provider,
    template,
    redirectSuccess,
    redirectError,
    logger=console,
    _dependencies={},
  } = params;
  if (_dependencies.recaptcha) recaptcha = _dependencies.recaptcha;
  if (_dependencies.mail) mail = _dependencies.mail;
  return async function (event, context, callback) {
    try {
      if (event.httpMethod !== 'POST') {
        return callback(null, { statusCode: 405, body: '' });
      }

      const body = querystring.parse(event.body);
      if (!body.name || !body.email || !body.message || !body['g-recaptcha-response']) {
        throw new HttpRequestError(400, ['InvalidBody']);
      }

      if (!isEmail(body.email)) {
        throw new HttpRequestError(400, ['InvalidEmail']);
      }

      const recaptchaBody = JSON.parse(await recaptcha(body['g-recaptcha-response']));
      if (!recaptchaBody.success) {
        logger.error(recaptchaBody['error-codes']);
        throw new HttpRequestError(429, ['RecaptchaError']);
      }

      const mailBody = await mail[provider]({
        from,
        to,
        subject,
        replyTo: { email: body.email, name: body.name },
        html: template(body),
      });
      if (mailBody.error) {
        logger.error(mailBody.error);
        throw new HttpRequestError(500, ['MailError']);
      }

      return callback(null, {
        statusCode: 303,
        headers: {
          Location: withQueryString(redirectSuccess, {
            contact: 'ok',
          }),
        },
      });
    } catch (err) {
      logger.error(err);
      const statusCode = err.statusCode || 500;
      const errorCodes = err.errorCodes || ['Internal'];
      return callback(null, {
        statusCode: 303,
        headers: {
          Location: withQueryString(redirectError, {
            contact: 'error',
            statusCode,
            errorCodes: JSON.stringify(errorCodes),
          }),
        },
      });
    }
  }
}
